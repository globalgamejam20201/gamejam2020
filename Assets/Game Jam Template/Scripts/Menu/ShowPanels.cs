﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class ShowPanels : MonoBehaviour
{

    public GameObject optionsPanel;                         //Store a reference to the Game Object OptionsPanel
    public GameObject optionsTint;                          //Store a reference to the Game Object OptionsTint
    public GameObject menuPanel;                            //Store a reference to the Game Object MenuPanel
    public GameObject pausePanel;                           //Store a reference to the Game Object PausePanel
    public GameObject gamePanel;

    private GameObject activePanel;
    private MenuObject activePanelMenuObject;
    private EventSystem eventSystem;

    private CanvasGroup menuCanvasGroup;
    private CanvasGroup menuPanelCanvasGroup;

    private void SetSelection (GameObject panelToSetSelected) {

        activePanel = panelToSetSelected;
        activePanelMenuObject = activePanel.GetComponent<MenuObject>();
        if (activePanelMenuObject != null) {
            activePanelMenuObject.SetFirstSelected();
        }
    }

    public void Start () {
        SetSelection(menuPanel);
        menuCanvasGroup = GetComponent<CanvasGroup>();
        menuPanelCanvasGroup = menuPanel.GetComponent<CanvasGroup>();
    }

    //Call this function to activate and display the Options panel during the main menu
    public void ShowOptionsPanel () {
        optionsPanel.SetActive(true);
        optionsTint.SetActive(true);
        menuPanelCanvasGroup.interactable = false;
        SetSelection(optionsPanel);

    }

    //Call this function to deactivate and hide the Options panel during the main menu
    public void HideOptionsPanel () {
        optionsPanel.SetActive(false);
        optionsTint.SetActive(false);
        menuPanelCanvasGroup.interactable = true;
        SetSelection(menuPanel);
    }

    //Call this function to activate and display the main menu panel during the main menu
    public void ShowMenu () {
        menuPanel.SetActive(true);
        gamePanel.SetActive(false);
        SetSelection(menuPanel);
    }

    //Call this function to deactivate and hide the main menu panel during the main menu
    public void HideMenu () {
        menuPanel.SetActive(false);
        gamePanel.SetActive(true);
        menuCanvasGroup.alpha = 1;
    }

    //Call this function to activate and display the Pause panel during game play
    public void ShowPausePanel () {
        pausePanel.SetActive(true);
        optionsTint.SetActive(true);
        SetSelection(pausePanel);
    }

    //Call this function to deactivate and hide the Pause panel during game play
    public void HidePausePanel () {
        pausePanel.SetActive(false);
        optionsTint.SetActive(false);
    }
}
