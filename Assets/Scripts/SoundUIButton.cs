﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SoundUIButton : Button
{
	public AudioClip SelectClip;
	public AudioClip SubmitClip;

	public override void OnSelect (BaseEventData eventData) {
		base.OnSelect(eventData);
		if (eventData is AxisEventData) {
			GamePlayManager.Instance.PlaySound(SelectClip);
		}
	}

	public override void OnSubmit (BaseEventData eventData) {
		base.OnSubmit(eventData);
		GamePlayManager.Instance.PlaySound(SubmitClip);
	}
}
