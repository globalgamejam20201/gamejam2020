﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GamePlayManager : MonoBehaviour
{
    public static GamePlayManager Instance { get; private set; }

    public StartOptions MenuStartOptions;

    public CarController Car;
    public WorkerPlayer Worker;

    public AudioClip EndClip;

    public bool IsPaused = false;
    public bool IsGameRunning = false;
    public bool IsGameEnding = false;

    public TextMeshProUGUI EndText;

    private Vector3 _startingCarPos = Vector3.zero;
    private Vector3 _startingWorkerPos = new Vector3(-6, 0, -3);

    private AudioSource _audioSource = null;

	public List<GameObject> LevelList;
	private int CurrentLevelIndex = 0;
	private GameObject CurrentLevel;

    private void Awake () {
        if (Instance == null) {
            Instance = this;
            _audioSource = GetComponent<AudioSource>();
        }
        else {
            Destroy(gameObject);
        }
    }

    public void StartGame () {
        _startingCarPos = Car.transform.position;

		LoadNextLevel();

        Car.StartEngine();

        IsGameRunning = true;
    }

    public void EndGame () {
        IsGameRunning = false;
        EndText.color = new Color(1f, 0.7179946f, 0.08962262f);
        EndText.text = "FAIL!!!";
        StartCoroutine(EndingGame(6f));
    }

    public void FinishedLevel () {
        EndText.color = new Color(0.29941297f, 1f, 0.29941297f);
        EndText.text = "Good Job!";
        StartCoroutine(FinishingLevel(3f));
    }

    public void PlaySound (AudioClip clip) {
        _audioSource.PlayOneShot(clip);
    }

	private IEnumerator FinishingLevel(float totalDuration)
	{
		IsGameEnding = true;
		StartCoroutine(FadeCanvasGroupAlpha(0f, 1f, 0.2f, MenuStartOptions.fadeOutImageCanvasGroup));

		MenuStartOptions.playMusic.FadeDown(totalDuration);

		CurrentLevelIndex++;

		yield return new WaitForSeconds(totalDuration);
		IsGameEnding = false;
		LoadNextLevel();

        StartCoroutine(FadeCanvasGroupAlpha(1f, 0f, 0.2f, MenuStartOptions.fadeOutImageCanvasGroup));
		MenuStartOptions.playMusic.FadeUp(0.2f);
        Car.StartEngine();
    }

	void FuckShitIHateGames()
	{
		// here ya go, chris

	}

	private IEnumerator EndingGame(float totalDuration)
	{

		StartCoroutine(FadeCanvasGroupAlpha(0f, 1f, 0.2f, MenuStartOptions.fadeOutImageCanvasGroup));
		PlaySound(EndClip);
		MenuStartOptions.playMusic.StopMusic();
		MenuStartOptions.playMusic.FadeDown(EndClip.length + 0.2f);

		CurrentLevelIndex = 0;
		LoadNextLevel();

		yield return new WaitForSeconds(totalDuration);
		MenuStartOptions.Restart();

		IsGameEnding = false;

	}

	void LoadNextLevel()
	{
		if (CurrentLevelIndex < LevelList.Count)
		{
			if (CurrentLevel)
			{
				Destroy(CurrentLevel);
			}
			CurrentLevel = Instantiate(LevelList[CurrentLevelIndex]);
			Car.transform.position = _startingCarPos;
			Car.ResetValues();
			Worker.ResetValues();
		}
	}

    private IEnumerator FadeCanvasGroupAlpha (float startAlpha, float endAlpha, float totalDuration, CanvasGroup canvasGroupToFadeAlpha) {
        float elapsedTime = 0f;
        while (elapsedTime < totalDuration) {
            elapsedTime += Time.deltaTime;
            float currentAlpha = Mathf.Lerp(startAlpha, endAlpha, elapsedTime / totalDuration);
            canvasGroupToFadeAlpha.alpha = currentAlpha;
            yield return null;
        }

        if (!IsGameEnding) {
            IsGameRunning = true;
        }
    }
}
