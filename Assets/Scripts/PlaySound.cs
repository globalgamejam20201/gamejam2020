﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySound : MonoBehaviour
{
    private AudioSource _soundSource;
    private const float RESET_TIME = 0.01f;
    private const float PITCH_TIME = 0.2f;

    void Awake () {
        _soundSource = GetComponent<AudioSource>();
    }

    public void PlaySelectedSound (AudioClip clipToPlay, bool loop = false, bool fade = false) {
        if (fade) {
            FadeOut(RESET_TIME);
        }

        _soundSource.clip = clipToPlay;
        _soundSource.loop = loop;

        _soundSource.Play();

        if (fade) {
            FadeIn(RESET_TIME);
        }
    }

    public void FadeIn (float fadeTime) {

		StartCoroutine(FadeVolume(_soundSource.volume, .08f, fadeTime));
    }

    public void FadeOut (float fadeTime) {
        StartCoroutine(FadeVolume(_soundSource.volume, 0f, fadeTime));
    }

    public void IncreasePitch (float pitch, bool instant = false) {
        StartCoroutine(FadePitch(_soundSource.pitch, pitch, instant));
    }

    public void DecreasePitch (float pitch, bool instant = false) {
        StartCoroutine(FadePitch(_soundSource.pitch, pitch, instant));
    }

    private IEnumerator FadeVolume (float startVol, float endVol, float totalDuration) {
        float elapsedTime = 0f;
        while (elapsedTime < totalDuration) {
            elapsedTime += Time.deltaTime;
            _soundSource.volume = Mathf.Lerp(startVol, endVol, elapsedTime / totalDuration);
            yield return null;
        }
    }

    private IEnumerator FadePitch (float startPitch, float endPitch, bool instant) {
        if (instant) {
            _soundSource.pitch = endPitch;
            yield return null;
        }
        else {
            float elapsedTime = 0f;
            while (elapsedTime < PITCH_TIME) {
                elapsedTime += Time.deltaTime;
                _soundSource.pitch = Mathf.Lerp(startPitch, endPitch, elapsedTime / PITCH_TIME);
                yield return null;
            }
        }
    }
}
