﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PotholeScript : MonoBehaviour
{
	public bool IsFilled = false;

	public Mesh FilledMesh;
	protected MeshFilter MyMeshFilter;

	public BoxCollider PotholeCollider;

    // Start is called before the first frame update
    void Start()
    {
		MyMeshFilter = GetComponent<MeshFilter>();
		PotholeCollider = GetComponent<BoxCollider>();
    }

	public void FillPothole()
	{
		IsFilled = true;
		if (MyMeshFilter && FilledMesh)
		{
			Destroy(PotholeCollider);
			MyMeshFilter.mesh = FilledMesh;
			transform.Translate(new Vector3(0, .1f));
		}
		else
		{
			print("Couldn't find Mesh and/or MeshFilter for this pothole");
		}

    }

    // This is dumb, but this script was here
    // So I added this, bitch!
    private void Update () {
        if (GamePlayManager.Instance.IsGameEnding) {
            StartCoroutine(DestroyMySelf());
        }
    }

    IEnumerator DestroyMySelf () {
        yield return new WaitForEndOfFrame();
        Destroy(gameObject);
    }
}
