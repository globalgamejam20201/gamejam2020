﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class MapInitializerScript : MonoBehaviour
{
	/*** Pothole initializaer ***/
	public GameObject PotholePrefab;
	// This should be at the left most corner of the starting grid
	public string PotholeDataFilepath = "";
	private int PlayerStartingDistance = 5;

	protected Vector3 StartingPosition;
	protected string[] PotholeData;
	protected int MaxRoadLength = 0;

	// Start is called before the first frame update
	void Start()
	{
		StartingPosition = transform.position;
		ParsePotholeData();
		SpawnPotholes();
		SetPlayerPositions();
		//CreateRandomSets(50);
		Destroy(this);
	}

	void SetPlayerPositions()
	{
		GameObject Worker = GameObject.Find("WorkerCharacter");
		WorkerPlayer WorkerScript = Worker.GetComponent<WorkerPlayer>();
		if (WorkerScript)
		{
			WorkerScript.RelativeHorizontalPosition.x = 0;
			//WorkerScript.MaxHorizontalGridSize = MaxRoadLength;

			Vector3 startpos = new Vector3(-6, 0, (PlayerStartingDistance * 6f));
			WorkerScript.transform.position = startpos;
		}


		GameObject Car = GameObject.Find("Car");
		CarController CarScript = Car.GetComponent<CarController>();
		if (CarScript)
		{
			CarScript.RelativeHorizontalPosition.x = 0;
			//CarScript.MaxHorizontalGridSize = MaxRoadLength - 1;
		}
	}

	void SpawnPotholes()
	{
		int PotholeLength = PotholeData.Length;
		Vector3 CurrentWorldPosition = StartingPosition;
		CurrentWorldPosition.y = .15f;
		bool spawnedFirstPothole = false;
		for (int i = 0; i < PotholeLength; i++)
		{
			char[] RowData = PotholeData[i].ToCharArray();
			CurrentWorldPosition.x = StartingPosition.x;
			CurrentWorldPosition.z += 6f;
			for (int rowIndex = 0; rowIndex < RowData.Length; rowIndex++)
			{
				if (RowData[rowIndex] == 'o')
				{
					if (!spawnedFirstPothole)
					{
						PlayerStartingDistance = i;
						spawnedFirstPothole = true;
					}
					Instantiate(PotholePrefab, CurrentWorldPosition, Quaternion.identity);
				}
				else if (RowData[rowIndex] == '-')
				{
					// don't do anything. leaving this here in case we want to do something with it
				}
				else
				{
				}

				CurrentWorldPosition.x += 6f;
			}
		}
	}

	void ParsePotholeData()
	{
		string text = @"---
---
---
---
---
---
---
---
---
---
---
---
---
---
---
---
---
---
---
---
---
---
---
---
---
---
---
---
---
---
---
---
---
---
---
---
---
---
---
---
---
---
---
---
oo-
---
---
---
-oo
---
---
---
oo-
---
---
---
-o-
---
---
---
---
---
---
---
o--
---
---
---
---
---
---
---
o--
---
---
---
---
---
---
---
---
---
---
---
---
---
---
---
-oo
---
---
---
o--
---
---
---
---
---
---
---
-o-
---
---
---
--o
---
---
---
--o
---
---
---
---
---
---
---
o--
---
---
---
---
---
---
---
-o-
---
---
---
---
---
---
---
--o
---
---
---
-o-
---
---
---
---
---
---
---
---
---
---
---
--o
---
---
---
o--
---
---
---
---
---
---
---
---
---
---
---
o--
---
---
---
---
---
---
---
---
---
---
---
-o-
---
---
---
o-o
---
---
---
-oo
---
---
---
---
---
---
---
oo-
---
---
---
oo-
---
---
---
-o-
---
---
---
o--
---
---
---
-o-
---
---
---
o--
---
---
---
--o
---
---
---
---
---
---
---
o--
---
---
---
-oo
---
---
---
o--
---
---
---
---
---
---
---
o-o
---
---
---
oo-
---
---
---
-oo
---
---
---
oo-
---
---
---
-o-
---
---
---
---
---
---
---
o--
---
---
---
---
---
---
---
o--
---
---
---
---
---
---
---
---
---
---
---
---
---
---
---
-oo
---
---
---
o--
---
---
---
---
---
---
---
-o-
---
---
---
--o
---
---
---
--o
---
---
---
---
---
---
---
o--
---
---
---
---
---
---
---
-o-
---
---
---
---
---
---
---
--o
---
---
---
-o-
---
---
---
---
---
---
---
---
---
---
---
--o
---
---
---
o--
---
---
---
---
---
---
---
---
---
---
---
o--
---
---
---
---
---
---
---
---
---
---
---
-o-
---
---
---
o-o
---
---
---
-oo
---
---
---
---
---
---
---
oo-
---
---
---
oo-
---
---
---
-o-
---
---
---
o--
---
---
---
-o-
---
---
---
o--
---
---
---
--o
---
---
---
---
---
---
---
o--
---
---
---
-oo
---
---
---
o--
---
---
---
---
---
---
---
o-o
---
---
---
oo-
---
---
---
-oo
---
---
---
oo-
---
---
---
-o-
---
---
---
---
---
---
---
o--
---
---
---
---
---
---
---
o--
---
---
---
---
---
---
---
---";
		char[] separators = { '\n' };
		PotholeData = text.Split(separators);
		MaxRoadLength = PotholeData[0].Length - 1;
	}


	void CreateRandomSets(int num)
	{
		int numHoles = Random.Range(0, 3);
		string master = "";

		int h = num;
		while (h > 0)
		{
			List<string> alpha = new List<string> { "-", "o", "-" };

			for (int i = 0; i < alpha.Count; i++)
			{
				string temp = alpha[i];
				int randomIndex = Random.Range(i, alpha.Count);
				alpha[i] = alpha[randomIndex];
				alpha[randomIndex] = temp;
			}

			string s = "";
			foreach (string asdf in alpha)
			{
				s += asdf;
			}
			master += s + "\n";
			h--;

			master += "---" + "\n";
			master += "---" + "\n";
			master += "---" + "\n";

		}
		print(master);
	}
}
