﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class WorkerPlayer : MonoBehaviour
{

	/*** Input ***/
	public Vector2 RelativeHorizontalPosition = new Vector2(0, 0);
	public int MaxHorizontalGridSize = 6;

	public string HorizontalInputName = "WorkerHorizontal";
	public string VerticalInputName = "WorkerVertical";
	public string FillInputName = "FillTest";
	public string DropInputName = "DropTest";
	public int MovementValue = 3;

	private bool bHorizontalInputDown = false;
	private bool bVerticalInputDown = false;

	/*** Pothole Interaction ***/
	public PotholeScript CurrentStandingPothole = null;
	public float PotholeRepairTime = 2f;
	private float CurrentFillTime = 0;
	private bool IsCurrentlyFillingPothole = false;

	/*** Particles ***/
	public ParticleSystem PotholeFillParticle;
	public ParticleSystem PotholeFillCompleteParticle;
	public ParticleSystem WorkerHitParticle1;
	public ParticleSystem WorkerHitParticle2;
	public ParticleSystem SlowdownPlacementParticle;
	public ParticleSystem SlowdownPowerupAddedParticle;


	/*** Slowdown dropping ***/
	public GameObject SlowdownPrefab;
	public int MaxSlowdownAmount = 3;
	public int FillStreakTillSlowdown = 5;
	private int CurrentSlowdownAmount = 3;
	private int CurrentHoleFillStreak = 0;


    public TextMeshProUGUI Powerups;
    public TextMeshProUGUI FilledPots;

    void Start()
	{
        Powerups.text = string.Format("x{0}", CurrentSlowdownAmount);
        FilledPots.text = string.Format("x{0}", CurrentHoleFillStreak);
    }

	void Update()
	{
        if (!GamePlayManager.Instance.IsGameRunning || GamePlayManager.Instance.IsPaused || GamePlayManager.Instance.IsGameEnding) {
            return;
        }

		HandleInput();
		HandleMoveInput();
		CheckCurrentObject();   // this would be a lot more efficient if we only called it on move or something
		UpdateFill();

        Powerups.text = string.Format("x{0}", CurrentSlowdownAmount);
        FilledPots.text = string.Format("x{0}", CurrentHoleFillStreak);
    }

	void CheckCurrentObject()
	{
		RaycastHit[] RayArray = Physics.RaycastAll(new Vector3(transform.position.x, transform.position.y + 10, transform.position.z), transform.TransformDirection(Vector3.down));
		bool foundPothole = false;
		foreach (RaycastHit RayHit in RayArray)
		{
			if (RayHit.transform.tag == "Pothole")
			{
				PotholeScript tempPothole = RayHit.transform.GetComponent<PotholeScript>();
				if (tempPothole)
				{
					foundPothole = true;
					CurrentStandingPothole = tempPothole;
					break;
				}
			}
		}
		// if we don't have a pothole, set it to none so we know he's not standing on anything
		if (!foundPothole)
		{
			CurrentStandingPothole = null;
		}
	}

	void HandleInput()
	{
		if (Input.GetButtonDown(FillInputName))
		{
			BeginFill();
		}
		else if (Input.GetButtonUp(FillInputName))
		{
			StopFill();
		}

		if (Input.GetButtonDown(DropInputName))
		{
			TryDropSlowdown();
		}
	}

	void TryDropSlowdown()
	{
		if (CurrentSlowdownAmount > 0)
		{
			CurrentSlowdownAmount--;

			if (SlowdownPrefab)
			{
				// spawn 'em
				Vector3 position = new Vector3(0, -.15f, transform.position.z);
				GameObject SlowdownObject = Instantiate(SlowdownPrefab, position, Quaternion.Euler(new Vector3(0, 180, 0)));
				if (SlowdownPlacementParticle)
				{
					SlowdownPlacementParticle.time = 0;
					SlowdownPlacementParticle.transform.position = position;
					SlowdownPlacementParticle.Play();
				}
			}
			else
			{
				print("no slowdown prefab to instantiate");
			}
		}
	}

	void TryAddSlowdown()
	{
		if (CurrentSlowdownAmount < MaxSlowdownAmount)
		{
			if (CurrentHoleFillStreak >= FillStreakTillSlowdown)
			{
				CurrentHoleFillStreak = 0;
				CurrentSlowdownAmount++;
				if (SlowdownPowerupAddedParticle)
				{
					SlowdownPowerupAddedParticle.time = 0;
					SlowdownPowerupAddedParticle.Play();
				}
			}
		}
		else
		{
			CurrentHoleFillStreak = 0;
		}
	}

	void UpdateFill()
	{
		if (IsCurrentlyFillingPothole)
		{
			CurrentFillTime += Time.deltaTime;
			// Play some filling anim/sound/particle here
			if (!CurrentStandingPothole)
			{
				StopFill();
			}
			else if (CurrentFillTime >= PotholeRepairTime)
			{
				CurrentStandingPothole.FillPothole();
				if (PotholeFillCompleteParticle)
				{
					PotholeFillCompleteParticle.time = 0;
					PotholeFillCompleteParticle.Play();
				}
				if (PotholeFillParticle)
				{
					PotholeFillParticle.Stop();
					PotholeFillParticle.Clear();
				}
				CurrentFillTime = 0;
				IsCurrentlyFillingPothole = false;
				CurrentStandingPothole = null;
				CurrentHoleFillStreak++;
				TryAddSlowdown();
				// play a sound/particle here
			}
		}
	}

	void BeginFill()
	{
		if (CurrentStandingPothole)
		{
			if (!CurrentStandingPothole.IsFilled)
			{
				CurrentFillTime = 0;
				IsCurrentlyFillingPothole = true;
				if (PotholeFillParticle)
				{
					PotholeFillParticle.time = 0;
					PotholeFillParticle.Play();
				}
			}
		}
	}

	void StopFill()
	{
		if (IsCurrentlyFillingPothole)
		{
			CurrentFillTime = 0;
			IsCurrentlyFillingPothole = false;
			if (PotholeFillParticle)
			{
				PotholeFillParticle.Stop();
				PotholeFillParticle.Clear();
			}
		}
	}

	void HandleMoveInput()
	{

		float horizontalVal = Input.GetAxis(HorizontalInputName);
		if (horizontalVal != 0)
		{
			if (!bHorizontalInputDown)
			{
				bHorizontalInputDown = true;

				if (horizontalVal > 0)
				{
					MoveRight();
				}
				else if (horizontalVal < 0)
				{
					MoveLeft();
				}
			}
		}
		else
		{
			bHorizontalInputDown = false;
		}

		float verticalVal = Input.GetAxis(VerticalInputName);
		if (verticalVal != 0)
		{
			if (!bVerticalInputDown)
			{
				bVerticalInputDown = true;

				if (verticalVal > 0)
				{
					MoveUp();
				}
				else if (verticalVal < 0)
				{
					MoveDown();
				}
			}
		}
		else
		{
			bVerticalInputDown = false;
		}

	}

	void MoveRight()
	{
		if (RelativeHorizontalPosition.x < MaxHorizontalGridSize - 1)
		{
			transform.Translate(new Vector3(MovementValue, 0, 0), Space.World);
			RelativeHorizontalPosition.x++;
		}
		transform.rotation = Quaternion.Euler(new Vector3(0, 90, 0));
	}

	void MoveLeft()
	{
		if (RelativeHorizontalPosition.x > 0)
		{
			transform.Translate(new Vector3(-MovementValue, 0, 0), Space.World);
			RelativeHorizontalPosition.x--;
		}
		transform.rotation = Quaternion.Euler(new Vector3(0, -90, 0));
	}

	void MoveUp()
	{
		transform.Translate(new Vector3(0, 0, MovementValue), Space.World);
		transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
	}

	void MoveDown()
	{
		transform.Translate(new Vector3(0, 0, -MovementValue), Space.World);
		transform.rotation = Quaternion.Euler(new Vector3(0, -180, 0));
	}

	public void GameLoss()
	{
		// get hit by a fuckin car 🚗🚗🚗🚗🚗
		print("OUCH MY BONES");
		if (WorkerHitParticle1)
		{
			WorkerHitParticle1.time = 0;
			WorkerHitParticle1.Play();
		}
		if (WorkerHitParticle2)
		{
			WorkerHitParticle2.time = 0;
			WorkerHitParticle2.Play();
		}
	}

	public void ResetValues () {
		transform.position = new Vector3(-6, 0, -3);
		RelativeHorizontalPosition = new Vector2(0, 0);
		//MaxHorizontalGridSize = 6;

		//MovementValue = 3;

		bHorizontalInputDown = false;
		bVerticalInputDown = false;

		CurrentStandingPothole = null;
		CurrentFillTime = 0;
		IsCurrentlyFillingPothole = false;

		// DALTON - DO THIS
		////////
	}

}
