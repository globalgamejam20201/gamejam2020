﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarController : MonoBehaviour
{
	public Vector2 RelativeHorizontalPosition = new Vector2(0, 0);
	public int MaxHorizontalGridSize = 5;

    public AudioClip CrashClip;
    public AudioClip EngineIdleClip;
    public AudioClip EngineGearClip;

    public string MoveLeftInputName = "CarLeft";
    public string MoveRightInputName = "CarRight";
    public int MovementValue = 3;
	public int SpeedIndex = 0;
	public float[] Speeds = { 10f, 20f, 30f, 50f };

	public float SpeedIncreaseTime = 5f;
    public float StartWaitTime = 3f;

    private float CurrentSpeedTimer = 0;

    private bool _inputDown = false;
    public bool _canMove = false;
    public bool _canSpeedUp = false;
    private PlaySound _playSound;

    // Start is called before the first frame update
    void Start()
    {
        _playSound = GetComponent<PlaySound>();
    }

	// Update is called once per frame
	void Update()
	{
        if (!_canMove || !GamePlayManager.Instance.IsGameRunning || GamePlayManager.Instance.IsPaused || GamePlayManager.Instance.IsGameEnding) {
            return;
        }

		UpdateSpeedTimer();
		UpdateMovement();
	}

	void UpdateSpeedTimer()
	{
        if (!_canSpeedUp) {
            return;
        }

		CurrentSpeedTimer += Time.deltaTime;
		if (CurrentSpeedTimer >= SpeedIncreaseTime)
		{
			IncreaseAcceleration();
		}
	}

	void UpdateMovement()
	{
		float horizontalVal = Input.GetAxis(MoveRightInputName) - Input.GetAxis(MoveLeftInputName);
		float SpeedVal = Speeds[SpeedIndex];
		Vector3 movement = new Vector3(0f, 0f, SpeedVal * Time.deltaTime);

		if (horizontalVal != 0)
		{
			if (!_inputDown)
			{
				_inputDown = true;

				if (horizontalVal > 0)
				{
					if (RelativeHorizontalPosition.x < MaxHorizontalGridSize - 1)
					{
						movement.x = MovementValue;
						RelativeHorizontalPosition.x++;
					}
				}
				else if (horizontalVal < 0)
				{
					if (RelativeHorizontalPosition.x > 0)
					{
						movement.x = -MovementValue;
						RelativeHorizontalPosition.x--;
					}
				}
			}
		}
		else
		{
			_inputDown = false;
		}

		transform.Translate(movement, Space.World);
	}

	private void OnTriggerEnter(Collider other)
	{
		if (!GamePlayManager.Instance.IsGameRunning || GamePlayManager.Instance.IsPaused || GamePlayManager.Instance.IsGameEnding) {
			return;
		}

        if (other.tag == "Powerup") {
            _canSpeedUp = false;
            DecreaseAcceleration();
        }
        else if (other.tag == "StartLine") {
            _canSpeedUp = true;
        }
        else if (other.tag == "FinishLine") {
            GamePlayManager.Instance.FinishedLevel();
        }
        else {
            StartCoroutine(GameLoss(other.gameObject));
        }
	}

    private void OnTriggerExit (Collider other) {
        if (!GamePlayManager.Instance.IsGameRunning || GamePlayManager.Instance.IsPaused || GamePlayManager.Instance.IsGameEnding) {
            return;
        }

        if (other.tag == "Powerup") {
            _canSpeedUp = true;
            Invoke("IncreaseAcceleration", SpeedIncreaseTime * 0.6f);
        }
    }

    IEnumerator GameLoss(GameObject hitObject)
	{
		GamePlayManager.Instance.IsGameEnding = true;
		SpeedIndex = 0;
        if (hitObject.tag == "Worker") {
			hitObject.GetComponent<WorkerPlayer>().GameLoss();
        }

        _playSound.DecreasePitch(1f, true);
        _playSound.PlaySelectedSound(CrashClip);

		yield return new WaitForSeconds(2f);

        // display UI here
        GamePlayManager.Instance.EndGame();
	}

	public void IncreaseAcceleration()
	{
		if (SpeedIndex < 3)
		{
			SpeedIndex++;
			_playSound.IncreasePitch(1 - SpeedIndex * 0.1f);
		}

		CurrentSpeedTimer = 0;
	}

    public void DecreaseAcceleration()
    {
        SpeedIndex = 0;
        CurrentSpeedTimer = 0;
        _playSound.IncreasePitch(1);
    }

    public void StartEngine () {
        _playSound.PlaySelectedSound(EngineIdleClip, false, true);

        StartCoroutine(StartDriving());
    }

	public void ResetValues () {
		RelativeHorizontalPosition = new Vector2(0, 0);
		SpeedIndex = 0;
		CurrentSpeedTimer = 0;
		_inputDown = false;
		_canMove = false;
        _canSpeedUp = false;
    }

    IEnumerator StartDriving () {
        yield return new WaitForSeconds(StartWaitTime);

        _canMove = true;
        _playSound.PlaySelectedSound(EngineGearClip, true, true);
    }
}
