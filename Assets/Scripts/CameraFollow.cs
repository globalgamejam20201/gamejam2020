﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform Target;

    private float _heightOffset;

    private void Start () {
        _heightOffset = transform.position.y;
    }

    // Update is called once per frame
    private void Update ()
    {
        Vector3 pos = Target.position;
        pos.y = _heightOffset;

        transform.position = pos;
    }
}
